/**

  NOTICE:
  The following features may require a polyfill for older browser support
  @see {@link http://caniuse.com/#feat=matchmedia}
  @see {@link http://caniuse.com/#feat=getcomputedstyle}

  USAGE:
  window.onresize = function(event) {
    console.log(mediaHelpers.isMobile());
    console.log(mediaHelpers.isTablet());
    console.log(mediaHelpers.isDesktop());
  };

*/
(function() {

  /**
   * Convert the first character of each word in a string to uppercase
   * @see {@link http://stackoverflow.com/a/10425344}
   * @param {String} input - The string to be converted
   * @returns {String}
   */
  function camelCase(input) {
    return input.toLowerCase().replace(/-(.)/g, function(match, char) {
      return char.toUpperCase();
    });
  }

  // Cutting the Mustard
  // document.querySelector - A large part of any JS library is its DOM selector. If the browser has native CSS selecting then it removes the need for a DOM selector. QuerySelector has been available in Firefox since 3.5 at least and has been working in webkit for ages. It also works in IE9.
  // window.addEventListener - Another large part of any JS library is event support. Every browser made in the last 6 years (except IE8) supports DOM level 2 events. If the browser supports this then we know it has better standards support than IE8.
  if ( 'querySelector' in document && 'addEventListener' in window ) {

    var queries = {},
        metaHelpers = {
          add: function(values) {
            for (var i = 0; i < values.length; i++) {
              var element = document.createElement("META");
              element.className = values[i];
              document.head.appendChild(element);
            }
          },
          get: function(value) {
            var element = document.head.querySelector('.' + value);
            var style = document.defaultView.getComputedStyle(element);
            var content = style.getPropertyValue('font-family');
            return content.replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g, '');
          }
        },
        selectors = [
          'mq-small-only',
          'mq-small-up',
          'mq-medium-down',
          'mq-medium-only',
          'mq-medium-up',
          'mq-large-down',
          'mq-large-only',
          'mq-large-up'
        ];

    // Wait for DOM to be ready
    document.addEventListener('DOMContentLoaded', function () {
      // Add some meta tags to the head so we can retrieve media query values from a style sheet
      metaHelpers.add(selectors);
      // Add a media query for each selector
      for (var i = 0; i < selectors.length; i++) {
        var selector = selectors[i];
        var key = camelCase(selector.replace('mq-', ''));
        queries[key] = metaHelpers.get(selector);
      }
      // Add media query methods to global scope
      window.mediaHelpers = {
        match:        function(mq) { return window.matchMedia(mq).matches; },
        isSmallOnly:  function() { return this.match(queries.smallOnly);   },
        isSmallUp:    function() { return this.match(queries.smallUp);     },
        isMediumDown: function() { return this.match(queries.mediumDown);  },
        isMediumOnly: function() { return this.match(queries.mediumOnly);  },
        isMediumUp:   function() { return this.match(queries.mediumUp);    },
        isLargeDown:  function() { return this.match(queries.largeDown);   },
        isLargeOnly:  function() { return this.match(queries.largeOnly);   },
        isLargeUp:    function() { return this.match(queries.largeUp);     },
        isMobile:     function() { return this.isSmallOnly();              },
        isTablet:     function() { return this.isMediumOnly();             },
        isDesktop:    function() { return this.isLargeUp();                }
      };
    });

  }

})();

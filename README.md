# Sass Base

A collection of sass functions and mixins for use in web development.

```
@import "sass-base/sass/functions"
@import "sass-base/sass/variables"
@import "sass-base/sass/mixins"
@import "sass-base/sass/meta"
@import "sass-base/sass/shared"
@import "sass-base/sass/page"
@import "sass-base/sass/type"
```

## Install

```
bower install --save-dev sass-base
```
